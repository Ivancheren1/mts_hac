﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerOff : MonoBehaviour
{
    public Text textTime;
    public float second = 0;
    private int minutes = 60;
    public int second2 = 0;
    bool plaing;
    public GameObject timerPanel;
    public GameObject obvodkaPanel;

    // Use this for initialization
    void Start()
    {
        timerPanel.SetActive(false);
        obvodkaPanel.SetActive(false);

    }
 

    // Update is called once per frame
    void Update()
    {
        if (plaing == true)

        {
            textTime.text = minutes + ":" + second2;
            second -= Time.deltaTime;
            second2 = (int)second;

            if (second < 1)
            {
                minutes--;
                second += 60f;
            }
            if (minutes == 0 && second2 == 1)
            {
                PlayerPrefs.SetInt("Gameover", 0);
                Application.LoadLevel("gameover");
            }
            if ((PlayerPrefs.GetInt("timer")) == 1)
            {
                second2 += 10;
            }
        }
    }
    public void startTimer()
    {
       plaing = true;
        timerPanel.SetActive(true);
        obvodkaPanel.SetActive(true);
    }
}