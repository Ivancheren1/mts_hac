﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public GameObject startPanel;
    public GameObject mainPanel;
    public GameObject PanelZadanie1;
    public GameObject PanelZadanie2;
    public GameObject PanelZadanie3;
    public GameObject PanelZadanie4;
    public GameObject PanelChatBot;

    void Start()
    {
        PanelZadanie1.SetActive(false);
        PanelZadanie2.SetActive(false);
        PanelZadanie3.SetActive(false);
        PanelZadanie4.SetActive(false);
        mainPanel.SetActive(false);
        PanelChatBot.SetActive(false);
        startPanel.SetActive(true);
    }
    public void startAPP()
    {
        startPanel.SetActive(false);
        mainPanel.SetActive(true);

    }
    public void ChatBottrue()
    {
        PanelChatBot.SetActive(true);
        startPanel.SetActive(false);
        mainPanel.SetActive(true);
    }
    public void ChatBotfaulse()
    {
        PanelChatBot.SetActive(false);
        mainPanel.SetActive(true);
    }

    public void Zadanie1ON()
    {
        PanelZadanie1.SetActive(true);
    }
    public void Zadanie1OFF()
    {
        PanelZadanie1.SetActive(false);
    }

    public void Zadanie2ON()
    {
        PanelZadanie2.SetActive(true);
    }
    public void Zadanie2OFF()
    {
        PanelZadanie2.SetActive(false);
    }

    public void Zadanie3ON()
    {
        PanelZadanie3.SetActive(true);
    }
    public void Zadanie3OFF()
    {
        PanelZadanie3.SetActive(false);
    }

    public void Zadanie4ON()
    {
        PanelZadanie4.SetActive(true);
    }
    public void Zadanie4OFF()
    {
        PanelZadanie4.SetActive(false);
    }



    void Update()
    {
        
    }
}
