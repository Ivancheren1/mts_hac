﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moiprofil : MonoBehaviour
{
    public GameObject moiprofilPanel;
    public GameObject novostiPanel;
    public GameObject levelsPanel;
    public GameObject mainPanel;
    public GameObject menuPanel;
    public Animation menuanime;
    public Animation menuanimeoff;

    public void startAPP()
    {
        moiprofilPanel.SetActive(true);
        mainPanel.SetActive(true);
    }

    public void vosvratnakartu()
    {
        moiprofilPanel.SetActive(false);
        novostiPanel.SetActive(false);
        levelsPanel.SetActive(false);
        mainPanel.SetActive(true);

    }
    public void levels()
    {
        moiprofilPanel.SetActive(false);
        novostiPanel.SetActive(false);
        levelsPanel.SetActive(true);
        mainPanel.SetActive(true);

    }
    public void menu()
    {
        moiprofilPanel.SetActive(true);
        novostiPanel.SetActive(false);
        levelsPanel.SetActive(false);
        mainPanel.SetActive(true);
        menuanime.CrossFade("menu");

    }
    public void menuoff()
    {
        moiprofilPanel.SetActive(false);
        novostiPanel.SetActive(false);
        levelsPanel.SetActive(false);
        mainPanel.SetActive(true);
        menuanimeoff.CrossFade("menuoff");

    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
